<?php

use controller\MoviesController;
use controller\UsersController;
use controller\PagesController;

class Router
{
    private $user_model_path;
    private $movie_model_path;
    private $controllers;

    public function __construct()
    {
        $this->controllers = array(
            "pages" => ["home", "error", "termsOfService", "privacyPolicy", "contactUs", "aboutUs"],
            "movies" => ["showMovies", "addMovie", "removeMovie", "removeAll", "showMovieDetails", "searchMovie"],
            "users" => ["signIn", "signUp", "profile", "signOut", "changePassword"] );

        $this->user_model_path = __DIR__ . "//models//user.php";
        $this->movie_model_path = __DIR__ . "//models//movie.php";
    }

    public function resolve($controller, $action)
    {
        if (array_key_exists($controller, $this->controllers)) {
            if (in_array($action, $this->controllers[$controller])) {
                $this->call($controller, $action);
            } else {
                $this->call("pages", "error");
            }
        } else {
            $this->call("pages", "error");
        }
    }

    public function call($controller, $action)
    {
        require_once(__DIR__ . "//controllers//". $controller ."_controller.php");

        switch ($controller) {
            case "users":
                require_once($this->user_model_path);
                $controller = new UsersController();
                break;
            case "pages":
                $controller = new PagesController();
                break;
            case "movies":
                require_once($this->user_model_path);
                require_once($this->movie_model_path);
                $controller = new MoviesController();
                break;
            default:
                require_once("views//pages//error.php");
        }

        $controller->{$action}();
    }
}

?>
