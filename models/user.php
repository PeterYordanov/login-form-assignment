<?php
namespace models;

class User
{
    private $id;
    private $username;
    private $email;
    private $confirmPassword;
    private $password;

    //Setters (accessor functions)
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setConfirmPassword($confirmPassword)
    {
        $this->confirmPassword = $confirmPassword;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    //Getters (accessor functions)
    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    public function getEmail()
    {
        return $this->email;
    }
}
