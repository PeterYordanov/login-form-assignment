<?php
namespace models;

class Movie
{
    private $id;
    private $name;
    private $premiereDate;
    private $location;
    private $description;

    //Setters (accessor functions)
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setTitle($name)
    {
        $this->name = $name;
    }

    public function setPremiereDate($premiereDate)
    {
        $this->premiereDate = $premiereDate;
    }

    public function setTheaterLocation($location)
    {
        $this->location = $location;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    //Getters (accessor functions)
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->name;
    }

    public function getPremiereDate()
    {
        return $this->premiereDate;
    }

    public function getTheaterLocation()
    {
        return $this->location;
    }

    public function getDescription()
    {
        return $this->description;
    }
}
