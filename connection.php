<?php
class Connection
{
    private $connection;
    private $username;
    private $password;

    function __construct()
    {
        $this->connection = null;
        $this->username = "root";
        $this->password = "";
    }

    public function getConnection()
    {
        if (!isset($this->connection)) {
            $this->connection = new PDO("mysql:host=localhost;dbname=login", $this->username, $this->password);
        }

        return $this->connection;
    }
}
?>
