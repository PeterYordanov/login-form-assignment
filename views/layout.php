<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login System Assignment</title>
    <meta name="viewport" content="width=device-width">

    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="221284692098-644n80sefmj2dbsu6nv0n0hj83qn8psp.apps.googleusercontent.com">
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>

    <script>
    function closeErrorMessage()
    {
        var cardMessage = document.getElementById("card");
        var cardContent = document.getElementById("card-content");
        cardMessage.removeChild(cardContent);
    }
    </script>

    <style>
    .fixed-navbar
    {
        top: 0;
        bottom: 0;
        position:fixed;
        overflow-y: hidden;
        overflow-x: hidden;
        z-index: 1;
    }

    .fixed-sidebar
    {
        top: 0;
        left: 100px;
        position: fixed;
        overflow-y: hidden;
        overflow-x: hidden;
        z-index: 1;
    }

    .button-spacing
    {
        margin-right: 5%;
    }

    .center-input
    {
        width: 50%;
        margin-left: 25%;
        text-align:center;
    }
    </style>

    <link rel="shortcut icon" type="image/x-icon" href="https://vum.bg/wp-content/uploads/2017/08/unnamed.png" />
</head>

<body>
<nav class="blue">
    <div class="navbar navbar-dark primary-color" id="task_flyout">
        <nav class="navbar navbar-expand-sm blue darken-2 fixed-navbar">
          <ul class="navbar-nav">
            <li class="nav-item" style="margin-left: 20%;">
                <a href="./" class="nav-link">Home</a>
            </li>
            <li class="nav-item">
                <a href="?controller=movies&action=showMovies" class="nav-link white-text text-darken-2">Movies list</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?controller=pages&action=termsOfService">Terms of Service</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?controller=pages&action=privacyPolicy">Privacy Policy</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?controller=pages&action=contactUs">Contact us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="?controller=pages&action=aboutUs">About us</a>
            </li>
          </ul>

        </nav>
    </div>
</nav>

<main class="main">
    <div class="container">
        <br>
        <div class="row">
            <div class="content col s10 m9">
                <?php
                    $routerPath = str_replace("views", "", __DIR__);
                    require_once($routerPath . "router.php");

                    $router = new \Router();
                    $router->resolve($controller, $action);
                ?>
            </div>

            <div class="sidebar col s12 m3">

                <div class="collection">
                    <div class="collection-item collection-header active">
                        <h6>
                            <i class="material-icons">account_box</i>
                             <?php echo isset($_SESSION["user"]) ? $_SESSION["user"] : "Guest" ?>
                        </h6>
                    </div>

                    <?php
                        if (!isset($_SESSION["id"])) {
                    ?>
                            <a href="?controller=users&action=signIn" class="collection-item blue-text text-darken-2"><i class="material-icons">exit_to_app</i>Sign-in</a>
                            <a href="?controller=users&action=signUp" class="collection-item blue-text text-darken-2"><i class="material-icons">create</i>Sign-up</a>
                    <?php
                        } else {
                    ?>
                            <a href="?controller=users&action=profile" class="collection-item blue-text text-darken-2"><i class="material-icons">supervisor_account</i>My profile</a>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</main>
<footer class="page-footer font-small blue pt-4">
    <div class="container">
      <div class="col l4 offset-l2 s12">
          <a href="https://vum.bg/"><img src="https://vum.bg/wp-content/uploads/2017/07/L.png"><br></a>
      </div>
      <div class="row">
            <div class="col l6 s12">
              <h5 class="white-text">Contact Us</h5>
              <ul>
                  <li><a class="white-text" href="#"><i class="material-icons">phone_callback</i>   (+359) 885 398663</a></li>
                  <li><a class="white-text" href="#"><i class="material-icons">sms</i>     peter.yordanov0@gmail.com</a></li>
              </ul>
            </div>

            <div class="col l6 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                    <li><a class="white-text" href="?controller=pages&action=termsOfService"><i class="material-icons">description</i>  Terms of Service</a></li>
                    <li><a class="white-text" href="?controller=pages&action=privacyPolicy"><i class="material-icons">fingerprint</i>  Privacy Policy</a></li>
                    <li><a class="white-text" href="?controller=pages&action=contactUs"><i class="material-icons">perm_phone_msg</i>  Contact Us</a></li>
                    <li><a class="white-text" href="?controller=pages&action=aboutUs"><i class="material-icons">how_to_reg</i>  About Us</a></li>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <i class="material-icons">copyright</i> All Rights Reserved.
        </div>
    </div>
</footer>
</body>
</html>
