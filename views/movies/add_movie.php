<div class="card">
    <form action="" method="post" class="form form-login">

        <div class="card-content">
            <h4 class="card-title">Add a Movie</h4><hr>

            <div class="row">
                <div class="input-field col s12">
                    <input name="movie_name" placeholder="Movie name" type="text" class="validate">
                </div>

                <div class="input-field col s12">
                    <input name="theater_location" placeholder="Theater location" id="location-field" type="text" class="validate">
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <textarea name="description" placeholder="Description" id="description-field" class="materialize-textarea validate"></textarea>
                </div>
            <script>
                $(document).ready(function () {
                    $('.datepicker').pickadate();
                })
            </script>
                <div class="input-field col s12">
                    <label class="gray-text" for="exam-date-field">Premiere Date</label>
                </div>

                <div class="input-field col s12">
                    <input name="premiere_date" type="date">
                </div>
            </div>

        </div>

        <div class="card-action">
            <input type="submit" name="submit" class="btn blue darken-2" value="Add">
        </div>
    </form>
</div>
