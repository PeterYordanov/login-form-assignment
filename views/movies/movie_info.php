<div class="collection">

    <div class="collection-item collection-header avatar active">
        <h5><?= $movie->getTitle(); ?></h5>
    </div>

    <div class="collection-item avatar">
        <i class="material-icons">place</i>
        <span class="title">Cinema location</span>
        <p><?= $movie->getTheaterLocation(); ?></p>
    </div>

    <div class="collection-item avatar">
        <i class="material-icons">calendar_today</i>
        <span class="title">Premiere date</span>
        <p><?= $movie->getPremiereDate(); ?></p>
    </div>

    <div class="collection-item avatar">
        <i class="material-icons">description</i>
        <span class="title">Description</span>
        <p class=""><?= $movie->getDescription(); ?></p>
    </div>

</div>
