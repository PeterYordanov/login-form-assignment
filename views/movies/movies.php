<script>
    function onChangeSubmit()
    {
        var submit = document.getElementById("search");
        submit.click();
    }
</script>

<div class="card">
    <form action="?controller=movies&action=searchMovie" method="post" class="form form-login">
        <div class="card-content">
            <h4 class="card-title" style="text-align: center;">Movies</h4><hr>

            <input type="text" placeholder="Search..." style="width: 50%;margin-left: 25%;text-align:center;" id="input" name="movieSearch" onchange="onChangeSubmit()">
            <input type="submit" style="visibility: hidden;" name="Search" id="search">

            <div class="collection">

                <?php
                    if(empty($movies)) {
                ?>
                            <div class="collection-item blue-text text-darken-2">
                                <p>No movies found with the name "<?= $movieSearch ?>"</p>
                            </div>
                <?php
                    }


                    for ($i = 0; $i < count($movies); $i++) {
                        $currentMovie = $movies[$i];
                ?>
                            <a href="?controller=movies&action=showMovieDetails&id=<?=$currentMovie["id"];?>" class="collection-item blue-text text-darken-2">
                                <i class="material-icons">local_movies</i>
                                <?= $currentMovie["movie_name"];?>
                                <label class="card-subtitle">(<?= $currentMovie["theater_location"];?>, <?= $currentMovie["premiere_date"]; ?>)</label>
                            </a>
                <?php
                    }
                ?>
            </div>

            <div class="card-action center">
            <?php
                if (isset($_SESSION["id"])) {
            ?>
                    <a href="?controller=movies&action=addMovie" class="btn blue darken-2">Add a Movie</a>
                    <a href="?controller=movies&action=removeMovie" class="btn blue darken-2">Remove a Movie</a>
                    <a href="?controller=movies&action=removeAll" class="btn blue darken-2">Remove all</a>
            <?php
                }
            ?>
            </div>
        </div>
    </form>
</div>
<br>
