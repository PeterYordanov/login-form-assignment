<div class="card">
    <form action="" method="post" class="form form-login">

        <div class="card-content">
            <h4 class="card-title">Remove a Movie</h4><hr>

            <div class="row">
                <div class="input-field col s12">
                    <input name="movie_name" placeholder="Movie name" type="text" class="validate">
                </div>
            </div>

            <div class="card-action">
                <input type="submit" name="submit" class="btn blue darken-2" value="Remove">
            </div>
        </div>
        
    </form>
</div>
