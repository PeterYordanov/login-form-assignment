<div class="card">
    <div class="card-content">
        <h4 class="card-title">Home</h4>
        <hr>
        <label>Assignment Description</label><br>
        <p>
            This is an assignment for first year in Web programming. April, 2019<br>
            It's a fully working login form system with the ability to manage a database of movies.<br>
            Currently, this assignment supports:<br>
            • Sign up<br>
            • Sign in<br>
            • Changing user password <br>
            • Seeing movies list<br>
            • Adding movies<br>
            • Removing movies <br>
            • Removing all movies <br>
            • Searching for movies<br>
            • Robustness and proper display of errors<br>
            • Contacting us
        </p>
    </div>
</div>
