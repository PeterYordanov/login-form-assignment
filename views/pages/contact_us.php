<div class="card">
    <form action="" method="post" class="form form-login">

        <div class="card-content">
            <h4 class="card-title">Contact us</h4>
            <label style="color: red">NOTICE:</label>
            <label>The following page won't work if it's ran locally. <br>
            One of the XAMPP modules (Mercury) is not configured to send emails to another server, as it's for debugging only.
            </label>
            <hr>
            <div class="row">
                <div class="input-field col s12">
                    <input name="email_from" placeholder="E-mail from" class="materialize-textarea validate">
                </div>

                <div class="input-field col s12">
                    <input name="subject" placeholder="Subject" type="text" class="validate">
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <textarea name="message" placeholder="Message" id="description-field" class="materialize-textarea validate"></textarea>
                </div>
            </div>
        </div>

        <div class="card-action center">
            <input type="submit" name="submit" class="btn blue darken-2" value="Send email">
        </div>

    </form>
</div>
