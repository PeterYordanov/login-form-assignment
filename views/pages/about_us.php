<div class="card">
    <div class="card-content">
        <p class="card-title">About us</p>
        <hr>
        <p>
            Welcome to Login Form Assignment, your number one source for all of your favourite movies. We're dedicated to providing you the very best of a list of movies, with an emphasis on simplicity, accessibility and being free of charge.<br>
            Founded in 2019 by Peter Yordanov, Login Form Assignment has come a long way from its beginnings.<br>
            We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, please don't hesitate to contact us.<br>
            <br>
            Sincerely,<br>
            Peter Yordanov
        </p>
    </div>
</div>
