<div class="card">
    <form action="" method="post" class="form form-login">
        <div class="card-content">
            <h4 class="card-title center">Sign in</h4><hr>

            <div class="row">

                <div class="input-field col s6">
                    <input name="username" placeholder="Username" id="username-field" type="text" class=".input-group">
                </div>

                <div class="input-field col s6">
                    <input name="password" placeholder="Password" id="password-field" type="password" class=".input-group-addon">
                </div>

            </div>

        </div>

        <div class="card-action center">
            <button type="submit" name="submit" class="btn blue darken-2">Sign-in</button>
        </div>

        <div class="userContent" style="display: none;"></div>
    </form>
</div>
