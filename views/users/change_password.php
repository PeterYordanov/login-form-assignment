<div class="card">
    <form action="" method="post" class="form form-login">
        <div class="card-content">
            <h4 class="card-title center">Change profile info</h4><hr>

            <div class="row">
                <div class="input-field col s12">
                    <input name="current_password" placeholder="Current password" id="username-field" type="password" class="validate center-input">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="input-field col s6">
                    <input name="new_password" placeholder="New Password" id="password-field" type="password" class="validate center-input">
                </div>

                <div class="input-field col s6">
                    <input name="confirm_new_password" placeholder="Confirm new password" id="password-confirm-field" type="password" class="validate center-input">
                </div>
            </div>
        </div>

        <div class="card-action center">
            <button type="submit" name="submit" class="btn blue darken-2">Change</button>
        </div>
    </form>
</div>
