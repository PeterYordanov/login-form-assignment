<div class="card">
    <form action="" method="post" class="form form-login">
        <div class="card-content">
            <h4 class="card-title center">Sign up</h4><hr>

            <div class="row">
                <div class="input-field col s6">
                    <input name="username" placeholder="Username" id="username-field" type="text" class="validate"
                           value="<?= $user->getUsername(); ?>">
                </div>

                <div class="input-field col s6">
                    <input name="email" placeholder="Email" id="email-field" type="email" class="validate"
                           value="<?= $user->getEmail(); ?>">
                </div>

                <div class="input-field col s6">
                    <input name="password" placeholder="Password" id="password-field" type="password"class="validate">
                </div>

                <div class="input-field col s6">
                    <input name="confirm_password" placeholder="Confirm password" id="password-confirm-field" type="password" class="validate">
                </div>
            </div>
        </div>

        <div class="card-action center">
            <button type="submit" name="submit" class="btn blue darken-2">Sign up</button>
        </div>
    </form>
</div>
