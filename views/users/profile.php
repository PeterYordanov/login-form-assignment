<div class="card">
    <div class="card-content">
        <h4 class="card-title"><?php echo "Welcome to your profile, ". $user->getUsername() ."!!"; ?></h4><hr>
        <p>
            <?php
                echo "<i class='material-icons'>account_box</i> Username: ". $user->getUsername(); echo "<br/>";
                echo "<i class='material-icons'>email</i> Email: ". $user->getEmail(); echo "<br/>";
            ?>
        </p>
        <br><br>
        <?php
            if(isset($_SESSION["id"])) {
         ?>
                <a href="?controller=users&action=signOut" class="btn blue darken-2">Sign out</a>
                <a href="?controller=users&action=changePassword" class="btn blue darken-2">Change password</a>
         <?php
            }
         ?>
    </div>
</div>
<br>
