<?php

namespace controller;

use models\Movie;

include_once("error_message.php");

class MoviesController
{
    public function searchMovie()
    {
        $db = new \Connection();
        $conn = $db->getConnection();

        $moviesQueryResult = $conn->query("SELECT * FROM `movies` WHERE movie_name LIKE '%". $_POST["movieSearch"] ."%';");
        $movies = $moviesQueryResult->fetchAll(\PDO::FETCH_ASSOC);
        $movieSearch = $_POST["movieSearch"];
        require_once("views/movies/movies.php");
    }

    public function removeMovie()
    {
        require_once("views/movies/remove_movie.php");
        $db = new \Connection();
        $conn = $db->getConnection();
        $movie = new Movie();

        if (isset($_SESSION["id"]) === null) {
            showErrorMessage("Please login first!");
        }

        if (isset($_POST["submit"])) {
            $movie->setTitle($_POST["movie_name"]);

            if(empty($movie->getTitle())) {
                showErrorMessage("Please don't leave the field empty!");
            } else {
                $delete = $conn->prepare("DELETE FROM movies WHERE movie_name='" . $movie->getTitle(). "'");
                $delete->execute();

                header("Location: ?controller=movies&action=showMovies");
            }
        }
    }

    public function removeAll()
    {
        $db = new \Connection();
        $conn = $db->getConnection();
        $movies = array();

        if (isset($_SESSION["id"]) === null) {
            showErrorMessage("Please login first!");
        } else {
            $delete = $conn->prepare("TRUNCATE TABLE movies");
            $delete->execute();
        }

        $movieSearch = "";
        require_once("views/movies/movies.php");
    }

    public function showMovies()
    {
        $db = new \Connection();
        $conn = $db->getConnection();

        $moviesQueryResult = $conn->query("SELECT * FROM movies");
        $movies = $moviesQueryResult->fetchAll(\PDO::FETCH_ASSOC);
        $movieSearch = "";
        require_once("views/movies/movies.php");
    }

    public function showMovieDetails()
    {
        $db = new \Connection();
        $conn = $db->getConnection();
        $movie = new Movie();
        $id = isset($_GET["id"]) ? $_GET["id"] : null;

        if($id == null) {
            header("Location: ./");
        }

        $movies = $conn->prepare("SELECT * FROM movies WHERE id=?");
        $movies->bindValue(1, $id);
        $movies->execute();
        $movieData = $movies->fetch(\PDO::FETCH_ASSOC);

        if ($movies->rowCount() !== 1) {
            showErrorMessage("Page not found");
            exit;
        }

        $movie->setTitle($movieData["movie_name"]);
        $movie->setTheaterLocation($movieData["theater_location"]);
        $movie->setDescription($movieData["description"]);
        $movie->setPremiereDate($movieData["premiere_date"]);

        require_once("views/movies/movie_info.php");
    }

    public function addMovie()
    {
        $db = new \Connection();
        $conn = $db->getConnection();
        $movie = new Movie();

        if (isset($_SESSION["id"]) === null) {
            showErrorMessage("Please login first!");
        }

        if (isset($_POST["submit"])) {
            $movie->setTitle($_POST["movie_name"]);
            $movie->setPremiereDate($_POST["premiere_date"]);
            $movie->setTheaterLocation($_POST["theater_location"]);
            $movie->setDescription($_POST["description"]);

            if (empty($movie->getTitle()) || empty($movie->getPremiereDate()) ||
                empty($movie->getTheaterLocation()) || empty($movie->getDescription())) {
                showErrorMessage("Please don't leave any fields empty!");
            } else {
                $stmt = $conn->prepare("INSERT INTO `movies`(`movie_name`, `premiere_date`, `description`, `theater_location`)
                                          VALUES (:movieName, :premiereDate, :description, :theaterLocation)");

                $startDate = new \DateTime($movie->getPremiereDate());
                $stmt->bindValue(":movieName", $movie->getTitle());
                $stmt->bindValue(":premiereDate", $startDate->format("d-m-Y"));
                $stmt->bindValue(":description", $movie->getDescription());
                $stmt->bindValue(":theaterLocation", $movie->getTheaterLocation());
                $stmt->execute();
                header("Location: ?controller=movies&action=showMovies");
            }
        }

        require_once("views/movies/add_movie.php");
    }
}
