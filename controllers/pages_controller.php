<?php

namespace controller;

include_once("error_message.php");

class PagesController
{
    public function home()
    {
        require_once("views/pages/home.php");
    }

    public function error()
    {
        require_once("views/pages/error.php");
    }

    public function privacyPolicy()
    {
        require_once("views/pages/privacy_policy.php");
    }

    public function termsOfService()
    {
        require_once("views/pages/terms_of_service.php");
    }

    public function contactUs()
    {
        $db = new \Connection();
        $conn = $db->getConnection();

        if (isset($_POST["submit"])) {
            mail("peter.yordanov0@gmail.com", $_POST["subject"], $_POST["message"], "From:" . $_POST["email_from"]);
        }
        require_once("views/pages/contact_us.php");
    }

    public function aboutUs()
    {
        require_once("views/pages/about_us.php");
    }
}

?>
