<?php

namespace controller;

use models\User;

include_once("error_message.php");

class UsersController
{
    public function changePassword()
    {
        require_once("views/users/change_password.php");

        $db = new \Connection();
        $conn = $db->getConnection();

        if (isset($_POST["submit"])) {
            $passHash = $conn->prepare("SELECT * FROM users WHERE username=:username");
            $passHash->bindValue(":username", $_SESSION["user"]);
            $passHash->execute();
            $userSession = $passHash->fetch(\PDO::FETCH_ASSOC);

            $user = new User();
            $user->setUsername($userSession["username"]);
            $user->setPassword($userSession["password"]);

            if(empty($_POST["current_password"]) || empty($_POST["new_password"]) || empty($_POST["confirm_new_password"])) {
                showErrorMessage("Don't leave any fields empty!");
            } else if (!password_verify($_POST["current_password"], $user->getPassword())) {
                showErrorMessage("Incorrect current password!");
            } else if(!($_POST["new_password"] === $_POST["confirm_new_password"])) {
                showErrorMessage("New password and confirm new password don't match!");
            } else {
                $statement = $conn->prepare("UPDATE users SET password=:password WHERE username=:username");
                $statement->bindValue(":username", $user->getUsername());
                $statement->bindValue(":password", password_hash($_POST["new_password"], PASSWORD_DEFAULT));
                $statement->execute();
            }
        }
    }

    public function signOut()
    {
        session_destroy();
        header("Location: ./");
    }

    public function signIn()
    {
        require_once("views/users/signin.php");
        $db = new \Connection();
        $conn = $db->getConnection();
        $user = new User();

        if (isset($_POST["submit"])) {
            $user->setUsername($_POST["username"]);
            $user->setPassword($_POST["password"]);

            $passHash = $conn->prepare("SELECT * FROM users WHERE username=:username");
            $passHash->bindValue(":username", $user->getUsername());
            $passHash->execute();
            $userSession = $passHash->fetch(\PDO::FETCH_ASSOC);

            if (empty($user->getUsername()) || empty($user->getPassword())) {
                showErrorMessage("Please don't leave any fields empty!");
            } elseif (!password_verify($user->getPassword(), $userSession["password"])) {
                showErrorMessage("Incorrect password!");
            } else {
                $_SESSION["id"] = $userSession["id"];
                $_SESSION["user"] = $userSession["username"];
                $_SESSION["email"] = $userSession["email"];
                header("Location: ./?controller=users&action=profile");
            }
        }
    }

    public function signUp()
    {

        $db = new \Connection();
        $conn = $db->getConnection();
        $user = new User();
        $sqlSelect = "SELECT * FROM users WHERE email=";
        $sqlInsert = "INSERT INTO users (`username`, `password`, `email`)";

        require_once("views/users/signup.php");

        if (isset($_POST["submit"])) {
            $user->setUsername($_POST["username"]);
            $user->setEmail($_POST["email"]);
            $user->setPassword($_POST["password"]);
            $user->setConfirmPassword($_POST["confirm_password"]);

            $checkEmail = $conn->prepare($sqlSelect . $user->getEmail());
            $checkEmail->execute();

            $fieldsAreEmpty = empty($user->getUsername()) && empty($user->getPassword()) &&
                              empty($user->getConfirmPassword()) && empty($user->getEmail());

            if ($fieldsAreEmpty) {
                showErrorMessage("Please don't leave any fields empty!");
            } elseif ($user->getPassword() !== $user->getConfirmPassword()) {
                showErrorMessage("Passwords don't match!");
            } elseif (strlen($user->getPassword()) < 6) {
                showErrorMessage("The password must be more than 6 characters!");
            } elseif ($checkEmail->rowCount() !== 0) {
                showErrorMessage("The email already exists!");
            } else {
                $insert = $conn->prepare($sqlInsert . "VALUES (:username, :password, :email)");
                $insert->bindValue(":username", $user->getUsername());
                $insert->bindValue(":password", password_hash($user->getPassword(), PASSWORD_DEFAULT));
                $insert->bindValue(":email", $user->getEmail());
                $insert->execute();
            }
        }
    }

    public function profile()
    {
        $user = new User();
        $db = new \Connection();
        $conn = $db->getConnection();
        $id = isset($_SESSION["id"]) ? $_SESSION["id"] : null;

        if ($id == null) {
            header("Location: ./");
        }

        $dbUser = $conn->prepare("SELECT * FROM users WHERE id=?");
        $dbUser->bindParam(1, $id);
        $dbUser->execute();
        $dbUser = $dbUser->fetch(\PDO::FETCH_ASSOC);

        $user->setUsername($dbUser["username"]);
        $user->setEmail($dbUser["email"]);

        require_once("views/users/profile.php");
    }
}
