-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2019 at 12:15 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` int(100) NOT NULL,
  `movie_name` varchar(100) NOT NULL,
  `premiere_date` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `theater_location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `movie_name`, `premiere_date`, `description`, `theater_location`) VALUES
(1, 'Jungle', '03-08-2017', 'In early 1980s, an Israeli adventurer named Yossi Ghinsberg travels to Bolivia planning to journey into the heart of the Amazon rainforest. There, he meets Marcus Stamm, a Swiss school teacher, and his friend, Kevin Gale, an American hiker and avid photographer. The three are staying in La Paz, where one of them has an apartment. Yossi is out in the market one day where a stranger asks if he is an American; Yossi replies, \"No\".\r\n\r\nDuring conversation the Austrian stranger, Karl Ruprechter, claims the existence of an Indian tribe in the jungle that they should go see. Karl says he knows the jungle, and he is friends with the tribe.\r\n\r\nYossi, excited about the prospect of exploring the uncharted jungle and meeting undiscovered peoples like the Toromonas, chooses to believe him. He heads back to the apartment to convince Marcus and Kevin to come along. Skeptical of the stranger and his story, they refuse. Yossi continues to press them until they ultimately acquiesce.\r\n\r\nThe next day, the trio meets Karl in a shop while he is gathering supplies for the hike. All three men are surprised when Karl leaves with the supplies and tells them they will pay for everything.\r\n\r\nYossi, Marcus, Kevin, and Karl hike through the jungle for several days. They make it to a village where it is apparent Karl knows the villagers. They spend the day in the village and stay overnight, then head back into the jungle the next morning.\r\n\r\nMarcus starts having trouble walking, and it is discovered his feet are full of bloody sores. By now, Kevin and Yossi are tired of Marcus\' complaining.\r\n\r\nYossi, Kevin, and Karl discuss how they all should proceed. Karl wants to leave the three while he goes and gets help, but Kevin and Yossi disagree with that plan. At this point Marcus walks up, and Kevin proposes building a raft to navigate downriver, so all of them can stay together.', 'Cinema City, Varna, Bulgaria'),
(2, 'Wilderness', '19-03-2006', 'The film starts with the arrival of Callum at a small juvenile facility ran by Jed and consists of Steve, Blue, Jethro, Lindsay, Davie and Lewis. It is implied that the group gives Davie and Lindsay a hard time and when Davie answers back to Steve\'s taunts Steve and Lewis humiliate the two by urinating on them. Callum laterdiscovers Davie has killed himself by slashing his wrists. Davie\'s dad learns about the suicide and the group, along with Jed, are sent to an island for team building exercises. Once arriving, the group notices something odd about it.\r\n\r\nCallum is knocked out by an unseen force and is discovered by the group. They go swimming and afterwards Jed notices a campfire in the distance. Lindsay is captured by an unseen force. They then meet a group of women, consisting of team leader Louise and her charges Jo and Mandy. Jed and Louise come to agreement to keep the groups separate, while Jo and Lewis form a romance.\r\n\r\nSteve and Lewis discover a homeless man and chase after him. The man attacks Steve with a stick in defence, prompting Steve to beat him with it and Callum later discovers him dead. Callum is caught by Jed and Louise washing the blood off his hands and is handcuffed to a tree. Jed discovers his emercency phone missing and blames the group for taking it which angers Steve. Jed and Louise decide to merge the groups for safety.\r\n\r\nThe next morning Jethro wakes up to go get more water and is then attacked by something. Jed orders Blue and Lindsay to go find him and they discover his dismembered arm in the water. The group is then ambushed and Jed shot with arrows to disable him and then disembowled a group of four dogs who act on command via a whistle. One of the dogs chases Louise and Callum but Louise sacrifices herself by leading the dogs away and she is knocked off a cliff.\r\n\r\nThe group discover Jethro\'s body hanging from a tree with the letter D carved into his flesh. They find a cabin and take refuge in itside. Lindsay reveals everything he knew, including who the killer is, which is Davie\'s dad. The next morning the group venture outside and Blue attempts to rape Jo enraging Lewis who attacks him and Blue runs away only to step on a bear trap and although Mandy and Callum attempt to free Blue he is killed after he falls face first into another trap. Davie\'s dad then forces the rest of them out of the cabin with smoke. As they make their escape Steve leaves Lindsay who is struggling to keep up.', 'Made-up Cinema, Plovdiv, Bulgaria'),
(3, 'Into the Wild', '21-09-2007', 'Into the Wild is a 2007 American biographical survival film written, co-produced, and directed by Sean Penn. It is an adaptation of Jon Krakauer\'s 1996 nonfiction book of the same name, based on the travels of Christopher McCandless across North America and his experiences in the Alaskan wilderness in the early 1990s. The film stars Emile Hirsch as McCandless, and Marcia Gay Harden and William Hurt as his parents, and features Jena Malone, Catherine Keener, Vince Vaughn, Kristen Stewart, and Hal Holbrook.\r\n\r\nThe film premiered during the 2007 Rome Film Fest and later opened outside Fairbanks, Alaska on September 21, 2007. It was nominated for two Golden Globes and won the award for Best Original Song: \"Guaranteed\" by Eddie Vedder. It was also nominated for two Academy Awards: Best Editing and Best Supporting Actor for Holbrook.', 'Kino Arena Grand Mall, Varna, Bulgaria'),
(4, 'Hacksaw Ridge', '04-09-2016', 'In rural 1920s Virginia, the young Desmond Doss nearly kills his little brother Hal while roughhousing. This event and his Seventh-day Adventist upbringing reinforce Desmond\'s belief in the commandment \"Thou shalt not kill\". Years later, Doss takes an injured man to the hospital and meets a nurse, Dorothy Schutte. The two strike a romance and Doss tells Dorothy of his interest in medical work.\r\n\r\nAfter the Japanese attack on Pearl Harbor, Doss enlists in the Army to serve as a combat medic. His father Tom, a World War I veteran, is deeply upset by the decision. Before leaving for Fort Jackson, he asks for Dorothy\'s hand in marriage and she accepts.\r\n\r\nDoss is placed in basic training under the command of Sergeant Howell. He excels physically, but becomes an outcast among his fellow soldiers for refusing to handle a rifle and train on Saturdays. Howell and Captain Glover attempt to discharge Doss for psychiatric reasons under Section 8 but are overruled, as Doss\' religious beliefs do not constitute mental illness. They subsequently torment Doss by putting him through grueling labor, intending to get Doss to leave of his own accord. Despite being beaten one night by his fellow soldiers, he refuses to identify his attackers and continues training.\r\n\r\nDoss\' unit completes training and is released on leave, during which Doss intends to marry Dorothy, but his refusal to carry a firearm leads to an arrest for insubordination. Captain Glover and Dorothy visit Doss in jail and try to convince him to plead guilty so that he can be released without charge, but Doss refuses to compromise his beliefs. At his court-martial Doss pleads not guilty, but before he is sentenced, his father barges into the tribunal with a letter from his former commanding officer (now a brigadier general) stating that his son\'s pacifism is protected by an Act of Congress. The charges against Doss are dropped, and he and Dorothy are married.', 'Made-up Cinema, Varna, Bulgaria'),
(5, 'Into the White', '04-03-2012', 'On 27 April 1940, a Luftwaffe Heinkel He 111 bomber (1H+CT) is pursued near Grotli by a Fleet Air Arm Blackburn Skua (L2940) fighter. Three of the four German crew survive the crash: pilot Leutnant Horst Schopis (Florian Lukas), Unteroffizier Josef Schwartz (David Kross) and Feldwebel Wolfgang Strunk (Stig Henrik Hoff). They prepare then set off for the coast to rejoin the fighting. A snowstorm develops. They loose their supplies and encounter a hunter\'s cabin. Two British airmen from the other downed aircraft, Capt. Charles P. Davenport (Lachlan Nieboer) and his air gunner Robert Smith (Rupert Grint), are heard to approach the cabin. They start to deal with each other in the cabin. An attempt in the morning, to set out for help, is abandoned when they start to get separated by the snowstorm.\r\n\r\nNorwegian soldiers set out as a patrol to look for the downed German airmen. They encounter Smith and Strunk skiing downhill. A Norwegian sniper fatally shoots Strunk and captures Smith.\r\n\r\nPortside, a Norwegian officer interrogates the Brits perceiving them collaborators. Davenport explains what happened and silences the interrogating officer.\r\n\r\nShopis and Schwartz are prisoners of war in Canada; Smith and Davenport return to combat action. Davenport survives the war', 'Kino Arena Grand Mall, Varna, Bulgaria');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`) VALUES
(9, 'Jon Snow', '$2y$10$bQmf/Vm.v.2j.HDLQVCkMuoHFsQBZ/TtD3tdo3lk1WyNBwnKTK0/m', 'jonsnow@gmail.com'),
(10, 'Jordan Peterson', '$2y$10$/DLI93EtZGkAEpSciaoQtuz3Kyr.9DdjKqETYnaTS7iv5g87h2Q5W', 'jbp@gmail.com'),
(11, 'Ben Shapiro', '$2y$10$va9czU9LIdcQQraYzQUeuOmydm9i6nE1ho9wRcE1OUYUV8LKJQmmK', 'benshapiro@gmail.com'),
(12, 'Peter Yordanov', '$2y$10$uhIhnPnJoEmEtn2QP2DtEuKImk/AE8opdrsPU3RN3BDicFeNwNsXi', 'peter.yordanov0@gmail.com'),
(13, 'Captain Jack Sparrow', '$2y$10$N4tNSiJWEEHwd1NXH7WwL.5CRnCjufDQL6Km2/vTK7CvCT13F80xS', 'jackyjack@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
