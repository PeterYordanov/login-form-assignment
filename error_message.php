<?php
function showErrorMessage($message)
{
    echo "<div class='card' id='card'>
        <div class='card-content' id='card-content'>
            <div class='row'>
                <div class='input-field col s6'>
                    <img src='http://financliga.ru/wp-content/uploads/2014/11/400px-Warning_icon.svg_.png' style='width:7%'>
                    <a>Error!</a>
                </div>

                <label class='input-field col m10' style='color:red;'>". $message ."</label>
            </div>
            <div class='card-action center'>
                <a href='.'><button class='btn blue darken-2'>Back to Home page</button></a>
                <button class='btn blue darken-2' onclick='closeErrorMessage()'>Close</button>
            </div>
        </div>
    </div>";
}
?>
